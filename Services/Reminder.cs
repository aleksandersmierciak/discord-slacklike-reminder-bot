﻿namespace SlackLikeReminderBot.Services
{
    public class Reminder
    {
        public Reminder(Recipient recipient, Message message)
        {
            Recipient = recipient;
            Message = message;
        }

        public Recipient Recipient { get; }

        public Message Message { get; }
    }
}
