﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System.Threading;
using System.Threading.Tasks;

namespace SlackLikeReminderBot.Services
{
    public class ReminderSchedulerService : IHostedService
    {
        private readonly DiscordSocketClient _discordSocketClient;
        private readonly IOptions<DiscordApi> _options;
        private readonly ILogger<ReminderSchedulerService> _logger;

        public ReminderSchedulerService(DiscordSocketClient discordSocketClient, IOptions<DiscordApi> options, ILogger<ReminderSchedulerService> logger)
        {
            _discordSocketClient = discordSocketClient;
            _options = options;
            _logger = logger;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation($"Starting {nameof(ReminderSchedulerService)}...");
            _discordSocketClient.MessageReceived += MessageReceivedAsync;
            await _discordSocketClient.LoginAsync(TokenType.Bot, _options.Value.Token);
            await _discordSocketClient.StartAsync();
            _logger.LogInformation($"{nameof(ReminderSchedulerService)} started successfully.");
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation($"Stopping {nameof(ReminderSchedulerService)}...");
            _discordSocketClient.Dispose();
            _logger.LogInformation($"{nameof(ReminderSchedulerService)} stopped successfully.");
            return Task.CompletedTask;
        }

        private async Task MessageReceivedAsync(SocketMessage rawMessage)
        {
            // Ignore system messages, or messages from other bots
            if (!(rawMessage is SocketUserMessage message)) return;
            if (message.Source != MessageSource.User) return;

            var argPos = 0;
            if (!message.HasMentionPrefix(_discordSocketClient.CurrentUser, ref argPos)) return;

            // TODO: validate input
            // TODO: schedule sending a reminder
            if (message.Content == "!ping")
                await message.Channel.SendMessageAsync("pong!");
        }
    }
}
