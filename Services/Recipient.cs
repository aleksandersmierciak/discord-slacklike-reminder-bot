﻿using System;

namespace SlackLikeReminderBot.Services
{
    public class Recipient
    {
        public Recipient(string value)
        {
            if (value is null) throw new ArgumentNullException("Recipient cannot be null", nameof(value));
            if (string.IsNullOrWhiteSpace(value)) throw new ArgumentException("Recipient cannot be empty", nameof(value));

            Value = value;
        }

        public string Value { get; }
    }
}
