﻿using System;

namespace SlackLikeReminderBot.Services
{
    public class Message
    {
        public Message(string value)
        {
            if (value is null) throw new ArgumentNullException("Message cannot be null", nameof(value));
            if (string.IsNullOrWhiteSpace(value)) throw new ArgumentException("Message cannot be empty", nameof(value));

            Value = value;
        }

        public string Value { get; }
    }
}
