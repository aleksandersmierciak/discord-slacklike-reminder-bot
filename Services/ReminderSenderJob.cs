﻿using Microsoft.Extensions.Logging;
using Quartz;
using System.Threading.Tasks;

namespace SlackLikeReminderBot.Services
{
    public class ReminderSenderJob : IJob
    {
        private readonly ILogger<ReminderSenderJob> _logger;

        public ReminderSenderJob(ILogger<ReminderSenderJob> logger)
        {
            _logger = logger;
        }

        public Task Execute(IJobExecutionContext context)
        {
            _logger.LogInformation($"{context.JobDetail.Key} job executing, triggered by {context.Trigger.Key}");
            // TODO: send a reminder according to data in context
            return Task.CompletedTask;
        }
    }
}
