﻿using Discord.WebSocket;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Quartz;
using SlackLikeReminderBot.Services;
using System;

namespace SlackLikeReminderBot.Console
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureServices((hostContext, services) =>
                {
                    services.Configure<DiscordApi>(hostContext.Configuration.GetSection("DiscordApi"));
                    services.AddTransient(_ => new DiscordSocketClient());
                    services.AddHostedService<ReminderSchedulerService>();
                    services.AddQuartz(q =>
                    {
                        q.UseMicrosoftDependencyInjectionScopedJobFactory();

                        // these are the defaults
                        q.UseSimpleTypeLoader();
                        q.UseInMemoryStore();
                        q.UseDefaultThreadPool(tp =>
                        {
                            tp.MaxConcurrency = 10;
                        });

                        // configure jobs with code
                        var jobKey = new JobKey("awesome job", "awesome group");
                        q.AddJob<ReminderSenderJob>(j => j
                            .StoreDurably()
                            .WithIdentity(jobKey)
                            .WithDescription("my awesome job")
                        );

                        q.AddTrigger(t => t
                            .WithIdentity("Simple Trigger")
                            .ForJob(jobKey)
                            .StartNow()
                            .WithSimpleSchedule(x => x.WithInterval(TimeSpan.FromSeconds(10)).RepeatForever())
                            .WithDescription("my awesome simple trigger")
                        );
                    });

                    services.AddQuartzServer(options =>
                    {
                        // when shutting down we want jobs to complete gracefully
                        options.WaitForJobsToComplete = true;
                    });
                });
    }
}